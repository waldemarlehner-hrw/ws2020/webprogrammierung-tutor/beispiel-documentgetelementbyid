Dieses Repo soll zeigen, wie man mit JS das Problem der nicht definierten Elemente über `document.getElementById(...)` gehebt.


[Datei mit Fehler], [Lösung 1] und [Lösung 2]

Die Quelldateien sind in /public aufzufinden.

[Datei mit Fehler]: https://waldemarlehner-hrw.gitlab.io/ws2020/webprogrammierung-tutor/beispiel-documentgetelementbyid/wrong.html
[Lösung 1]: https://waldemarlehner-hrw.gitlab.io/ws2020/webprogrammierung-tutor/beispiel-documentgetelementbyid/fix1.html
[Lösung 2]: https://waldemarlehner-hrw.gitlab.io/ws2020/webprogrammierung-tutor/beispiel-documentgetelementbyid/fix2.html